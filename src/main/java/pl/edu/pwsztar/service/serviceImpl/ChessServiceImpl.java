package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rook;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rook,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rook = rook;
        this.pawn = pawn;
    }

    private Integer convertCharToInt(String value){
        Map<String, Integer> position = new LinkedHashMap<>();
        position.put("a", 1);
        position.put("b", 2);
        position.put("c", 3);
        position.put("d", 4);
        position.put("e", 5);
        position.put("f", 6);
        position.put("g", 7);
        position.put("h", 8);

        return position.get(value);
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        String[] startPosition = figureMoveDto.getStart().split("_");
        String[] destinationPosition = figureMoveDto.getDestination().split("_");

        int xStart = convertCharToInt(startPosition[0]);
        int yStart = Integer.parseInt(startPosition[1]);
        int xEnd = convertCharToInt(destinationPosition[0]);
        int yEnd = Integer.parseInt(destinationPosition[1]);

        switch (figureMoveDto.getType()){
            case BISHOP:
                return bishop.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KNIGHT:
                return knight.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KING:
                return king.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case QUEEN:
                return queen.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case ROCK:
                return rook.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case PAWN:
                return pawn.isCorrectMove(xStart, yStart, xEnd, yEnd);
            default:
                return false;
        }
    }
}
